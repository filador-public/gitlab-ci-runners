# GitLab CI Runners

In this repository, you can deploy two kinds of Runner with Terraform : standard and autoscaler.

You can refer to the french article [here](https://blog.filador.fr/installation-de-runners-avec-gitlab-ci/).
