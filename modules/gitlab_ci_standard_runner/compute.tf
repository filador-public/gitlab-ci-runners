locals {
  gitlab_runner_name = "ci-gitlab-runner-${var.zone}-${var.runner_name_suffix}"
}

resource "google_compute_instance" "gitlab_runner" {
  project = var.project_id

  name         = local.gitlab_runner_name
  machine_type = var.virtual_machine_type
  zone         = var.zone

  allow_stopping_for_update = true

  tags = ["gitlab-runner"]

  boot_disk {
    initialize_params {
      image = var.virtual_machine_image
    }
  }

  network_interface {
    subnetwork         = var.subnetwork_name
    subnetwork_project = var.project_id
  }

  service_account {
    email  = var.service_account_email
    scopes = ["cloud-platform"]
  }

  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }

  metadata = {
    app  = "gitlab-runner",
    type = "standard",
    shutdown-script = templatefile("${path.module}/files/gitlab-runner-unregister.sh", {
      gitlab_runner_name = local.gitlab_runner_name
    })
  }

  metadata_startup_script = templatefile(
    "${path.module}/files/gitlab-runner-register.sh", {
      gitlab_server_url         = var.gitlab_server_url
      gitlab_runner_name        = local.gitlab_runner_name
      secret_manager_token_name = var.secret_manager_token_name
  })
}
