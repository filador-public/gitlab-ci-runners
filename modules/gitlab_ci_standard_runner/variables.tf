variable "project_id" {
  type        = string
  description = "Google Cloud project ID"
}

variable "region" {
  type        = string
  description = "Google Cloud region"
}

variable "zone" {
  type        = string
  description = "Google Cloud availability zone"
}

variable "virtual_machine_type" {
  type        = string
  description = "Compute instance type for GitLab Runner"
}

variable "virtual_machine_image" {
  type        = string
  description = "Operating System for GitLab Runner"
}

variable "subnetwork_name" {
  type        = string
  description = "Subnetwork name"
}

variable "service_account_email" {
  type        = string
  description = "Email for GitLab Runner service account"
}

variable "runner_name_suffix" {
  type        = string
  description = "Runner name suffix"
}

variable "gitlab_server_url" {
  type        = string
  description = "Url of GitLab Server to register Runner"
}

variable "secret_manager_token_name" {
  type        = string
  description = "Token name to retrieve in Secret Manager"
}
