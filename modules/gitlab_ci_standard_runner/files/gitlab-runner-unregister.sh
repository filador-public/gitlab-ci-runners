#!/bin/bash

# Unregister GitLab Runner
gitlab-runner unregister --name "${gitlab_runner_name}"
