#!/bin/bash

# Configure GitLab Repository
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

#  Install Docker and GitLab Runner packages
apt-get update
apt-get install docker gitlab-runner -y

# Retrieve GitLab token from Secret Manager
gitlab_runner_token=$(gcloud secrets versions access latest \
  --secret="${secret_manager_token_name}" \
  --format='get(payload.data)' | tr '_-' '/+' | base64 -d)

# Configure GCR
yes | gcloud auth configure-docker eu.gcr.io

# Remove unhealthy GitLab Runner
gitlab-runner verify --delete

# Register GitLab Runner
gitlab-runner register \
  --non-interactive \
  --name="${gitlab_runner_name}" \
  --url="https://${gitlab_server_url}/" \
  --token="$${gitlab_runner_token}" \
  --executor="docker" \
  --limit=3 \
  --docker-image="scratch:latest" \
  --docker-privileged=false \
  --docker-disable-cache=true \
