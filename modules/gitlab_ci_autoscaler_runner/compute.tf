locals {
  gitlab_runner_name = "ci-gitlab-runner-as-${var.zone}-${var.runner_name_suffix}"
}

resource "google_compute_instance" "gitlab_runner_autoscaler" {
  project = var.project_id

  name         = local.gitlab_runner_name
  machine_type = var.virtual_machine_type
  zone         = var.zone

  allow_stopping_for_update = true

  tags = ["gitlab-runner-autoscaler"]

  boot_disk {
    initialize_params {
      image = var.virtual_machine_image
    }
  }

  network_interface {
    subnetwork         = var.subnetwork_name
    subnetwork_project = var.project_id
  }

  service_account {
    email  = google_service_account.gitlab_runner_autoscaler.email
    scopes = ["cloud-platform"]
  }

  shielded_instance_config {
    enable_secure_boot          = true
    enable_vtpm                 = true
    enable_integrity_monitoring = true
  }

  metadata = {
    app  = "gitlab-runner",
    type = "autoscaler",
    shutdown-script = templatefile("${path.module}/files/gitlab-runner-unregister.sh", {
      gitlab_runner_name = local.gitlab_runner_name
    })
  }

  metadata_startup_script = templatefile(
    "${path.module}/files/gitlab-runner-register.sh", {
      gitlab_server_url                    = var.gitlab_server_url
      gitlab_runner_name                   = local.gitlab_runner_name
      gitlab_runner_name_suffix            = var.runner_name_suffix
      secret_manager_token_name            = var.secret_manager_token_name
      gitlab_runner_google_project         = var.project_id
      gitlab_runner_google_network         = var.network_name
      gitlab_runner_google_subnetwork      = var.subnetwork_name
      gitlab_runner_google_zone            = var.zone
      gitlab_runner_google_service_account = var.service_account_email
  })
}

resource "google_compute_firewall" "docker-machine" {
  project = var.project_id

  name    = "docker-machines"
  network = var.network_name

  allow {
    protocol = "tcp"
    ports    = ["22", "2376"]
  }

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }

  source_tags = ["gitlab-runner-autoscaler"]
  target_tags = ["gitlab-runner"]
}