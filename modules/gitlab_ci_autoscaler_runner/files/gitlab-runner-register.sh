#!/bin/bash

# Configure GitLab Repository
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

#  Install Docker and GitLab Runner packages
apt-get update
apt-get install docker gitlab-runner -y

# Configure Docker Machine
curl -O "https://gitlab-docker-machine-downloads.s3.amazonaws.com/v0.16.2-gitlab.21/docker-machine-Linux-x86_64"
cp docker-machine-Linux-x86_64 /usr/local/bin/docker-machine
chmod +x /usr/local/bin/docker-machine

# Retrieve GitLab token from Secret Manager
gitlab_runner_token=$(gcloud secrets versions access latest \
  --secret="${secret_manager_token_name}" \
  --format='get(payload.data)' | tr '_-' '/+' | base64 -d)

# Configure GCR
yes | gcloud auth configure-docker eu.gcr.io

# Remove unhealthy GitLab Runner
gitlab-runner verify --delete

# Register GitLab Runner
gitlab-runner register \
  --non-interactive \
  --name="${gitlab_runner_name}" \
  --url="https://${gitlab_server_url}/" \
  --token="$${gitlab_runner_token}" \
  --executor="docker+machine" \
  --limit=20 \
  --docker-image="scratch:latest" \
  --docker-privileged=false \
  --docker-disable-cache=true \
  --machine-machine-driver=google \
  --machine-machine-name="${gitlab_runner_name_suffix}-%s" \
  --machine-idle-count-min=0 \
  --machine-idle-time=400 \
  --machine-max-builds=100 \
  --machine-machine-options=google-project=${gitlab_runner_google_project} \
  --machine-machine-options=google-network=${gitlab_runner_google_network} \
  --machine-machine-options=google-subnetwork=${gitlab_runner_google_subnetwork} \
  --machine-machine-options=google-machine-type=n1-standard-1 \
  --machine-machine-options=google-service-account=${gitlab_runner_google_service_account} \
  --machine-machine-options=google-scopes=https://www.googleapis.com/auth/cloud-platform \
  --machine-machine-options=google-zone=${gitlab_runner_google_zone} \
  --machine-machine-options=google-use-internal-ip=true \
  --machine-machine-options=google-use-internal-ip-only=true \
  --machine-machine-options=google-tags=gitlab-runner \
  --machine-machine-options=google-username=cosadmin \
  --machine-machine-options=google-metadata=enable-oslogin=FALSE \
  --machine-machine-options=google-machine-image=cos-cloud/global/images/cos-stable-105-17412-156-23 \
  --machine-machine-options=google-preemptible=true \
