resource "google_service_account" "gitlab_runner_autoscaler" {
  project = var.project_id

  account_id   = "sac-gitlab-runner-autoscaler"
  display_name = "GitLab Runner autoscaler service account"
}

resource "google_project_iam_member" "gitlab_runner_autoscaler" {
  for_each = var.gitlab_runner_autoscaler_roles

  project = var.project_id

  role   = each.value
  member = "serviceAccount:${google_service_account.gitlab_runner_autoscaler.email}"
}

resource "google_project_iam_custom_role" "docker_machine" {
  project = var.project_id

  role_id     = "dockerMachine.admin"
  title       = "Docker Machine Admin"
  description = "A Docker Machine admin role"
  permissions = [
    "compute.firewalls.update",
    "compute.networks.updatePolicy",
  ]
}

resource "google_project_iam_member" "docker_machine" {
  project = var.project_id

  role   = google_project_iam_custom_role.docker_machine.name
  member = "serviceAccount:${google_service_account.gitlab_runner_autoscaler.email}"
}
