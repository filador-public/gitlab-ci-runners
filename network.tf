resource "google_compute_network" "runner" {
  project = var.project_id

  name                    = "vpc-runner"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "runner" {
  project = var.project_id

  name          = "sn-runner-${var.region}"
  ip_cidr_range = "10.0.0.0/24"
  region        = var.region
  network       = google_compute_network.runner.id

  private_ip_google_access = true
}

resource "google_compute_router" "runner" {
  project = var.project_id

  name = "cr-runner"

  region  = var.region
  network = google_compute_network.runner.id
}

resource "google_compute_router_nat" "runner" {
  project = var.project_id

  name = "nat-runner"

  router                             = google_compute_router.runner.name
  region                             = google_compute_router.runner.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}