terraform {
  required_version = ">= 1.4"

  required_providers {
    google = {
      source  = "google"
      version = "~> 4.66.0"
    }
  }

  backend "gcs" {}
}

provider "google" {}