module "runner" {
  source = "./modules/gitlab_ci_standard_runner"

  project_id = var.project_id
  region     = var.region
  zone       = var.zone

  virtual_machine_type  = "e2-medium"
  virtual_machine_image = "debian-cloud/debian-11"
  subnetwork_name       = google_compute_subnetwork.runner.name
  service_account_email = "" # À compléter

  runner_name_suffix        = "filador"
  gitlab_server_url         = "gitlab.com"
  secret_manager_token_name = "" # À compléter
}

module "autoscaler_runner" {
  source = "./modules/gitlab_ci_autoscaler_runner"

  project_id = var.project_id
  region     = var.region
  zone       = var.zone

  virtual_machine_type  = "e2-micro"
  virtual_machine_image = "debian-cloud/debian-11"
  network_name          = google_compute_network.runner.name
  subnetwork_name       = google_compute_subnetwork.runner.name
  service_account_email = "" # À compléter

  runner_name_suffix        = "filador"
  gitlab_server_url         = "gitlab.com"
  secret_manager_token_name = "" # À compléter
}
